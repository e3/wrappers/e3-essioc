# e3-essioc

Wrapper for the module essioc.

See also
- [autosave](https://gitlab.esss.lu.se/e3/wrappers/e3-autosave)
- [caputlog](https://gitlab.esss.lu.se/e3/wrappers/e3-caPutLog)
- [iocstats](https://gitlab.esss.lu.se/e3/wrappers/e3-iocStats)
- [recsync](https://gitlab.esss.lu.se/e3/wrappers/e3-recsync)

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh -r "essioc"
```

## Contributing

Contributions through pull/merge requests only.
